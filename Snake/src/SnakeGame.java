import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.*;

/**
 * @class SnakeGame
 * @brief A class representing the snake game panel.
 * @extends JPanel
 * @implements ActionListener, KeyListener
 */
public class SnakeGame extends JPanel implements ActionListener, KeyListener {
    int boardHeight, boardWidth; /**< The height and width of the game board */
    Snake snake; /**< The player's snake */
    SnakeAI snakeAI; /**< The AI-controlled snake */
    Food food; /**< The food item on the board */
    ArrayList<Wall> walls; /**< The walls on the game board */
    Timer gameLoop; /**< Timer for the game loop */
    LeaderBoard leaderBoard = new LeaderBoard(helperVariables.leaderBoardFileName); /**< LeaderBoard instance */
    boolean isGameOver = false; /**< Game over flag */
    int velocityX, velocityY; /**< The velocity of the player's snake */
    Random random; /**< Random instance for generating positions */

    /**
     * @brief Constructor to initialize the snake game with the board dimensions.
     * @param boardWidth The width of the game board.
     * @param boardHeight The height of the game board.
     * @throws IOException if an I/O error occurs.
     */
    SnakeGame(int boardWidth, int boardHeight) throws IOException {
        this.boardHeight = boardHeight;
        this.boardWidth = boardWidth;

        setBackground(Color.black);
        addKeyListener(this);
        setFocusable(true);

        velocityX = 0;
        velocityY = 0;

        snake = new Snake(5, 5);

        random = new Random();
        food = new Food(10, 10);
        walls = new ArrayList<>();
        String dir;
        for (int i = 0; i < 5; ++i) {
            if (i % 2 == 1) {
                dir = "vertical";
            } else {
                dir = "horizontal";
            }
            walls.add(new Wall(i + 3, dir));
        }
        walls.add(new Wall(10, "horizontal"));
        walls.add(new Wall(12, "horizontal"));
        walls.add(new Wall(8, "vertical"));
        walls.add(new Wall(8, "vertical"));

        snakeAI = new SnakeAI(this);
        placeFood();

        gameLoop = new Timer(100, this);
        gameLoop.start();
    }

    /**
     * @brief Requests focus for the game panel.
     */
    public void requestFocusInSnakeGamePanel() {
        this.requestFocusInWindow();
    }

    /**
     * @brief Generates a random starting position for the snake.
     * @return A SnakeSegment representing the random start position.
     */
    public SnakeSegment generateRandomStartPosition() {
        int x, y;
        boolean positionIsValid;
        do {
            positionIsValid = true;
            x = random.nextInt(boardWidth / helperVariables.tileSize);
            y = random.nextInt((boardHeight / helperVariables.tileSize));

            for (Wall wallObject : walls) {
                for (WallSegment wallPart : wallObject.wall) {
                    if (wallPart.x == x && wallPart.y == y) {
                        positionIsValid = false;
                        break;
                    }
                }
                if (!positionIsValid) break;
            }
        } while (!positionIsValid);

        return new SnakeSegment(x, y);
    }

    /**
     * @brief Paints the game components on the panel.
     * @param g The Graphics object to draw on.
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        draw(g);
    }

    /**
     * @brief Draws the game components on the panel.
     * @param g The Graphics object to draw on.
     */
    public void draw(Graphics g) {
        for (int i = 0; i < this.boardWidth / helperVariables.tileSize; ++i) {
            g.setColor(Color.darkGray);
            g.drawLine(i * helperVariables.tileSize, 0, i * helperVariables.tileSize, boardHeight);
            g.drawLine(0, i * helperVariables.tileSize, boardWidth, i * helperVariables.tileSize);
        }
        snake.draw(g);
        snakeAI.draw(g);
        for (Wall wall : walls) {
            wall.draw(g);
        }
        g.setColor(Color.red);
        g.fillRect(food.x, food.y, helperVariables.tileSize, helperVariables.tileSize);

        g.setColor(Color.orange);
        g.setFont(new Font("Arial", Font.PLAIN, 16));
        if (!isGameOver) {
            g.drawString("Score: " + snake.snakeBody.size(), helperVariables.tileSize - 16, helperVariables.tileSize);
        }
    }

    /**
     * @brief Generates a random position for the food item.
     * @return A Food object with a random position.
     */
    private Food generateRandomFoodPosition() {
        boolean positionIsValid;
        int x, y;
        do {
            positionIsValid = true;
            x = random.nextInt(boardWidth / helperVariables.tileSize);
            y = random.nextInt((boardHeight / helperVariables.tileSize));

            for (Wall wallObject : walls) {
                for (WallSegment wallPart : wallObject.wall) {
                    if (wallPart.x / helperVariables.tileSize == x && wallPart.y / helperVariables.tileSize == y) {
                        positionIsValid = false;
                        break;
                    }
                }
                if (!positionIsValid) break;
            }

            for (SnakeSegment snakeSegment : snake.snakeBody) {
                if (snakeSegment.x / helperVariables.tileSize == x && snakeSegment.y / helperVariables.tileSize == y) {
                    positionIsValid = false;
                    break;
                }
            }
            for (SnakeSegment snakeSegment : snakeAI.snakeBody) {
                if (snakeSegment.x / helperVariables.tileSize == x && snakeSegment.y / helperVariables.tileSize == y) {
                    positionIsValid = false;
                    break;
                }
            }

            if (snake.snakeHead.x / helperVariables.tileSize == x && snake.snakeHead.y / helperVariables.tileSize == y) {
                positionIsValid = false;
            }
            if (snakeAI.snakeHead.x / helperVariables.tileSize == x && snakeAI.snakeHead.y / helperVariables.tileSize == y) {
                positionIsValid = false;
            }
        } while (!positionIsValid);

        return new Food(x, y);
    }

    /**
     * @brief Places the food item at a random position on the board.
     */
    public void placeFood() {
        food = generateRandomFoodPosition();
    }

    /**
     * @brief Gets the height of the game board.
     * @return The height of the game board.
     */
    public int getHeight() {
        return this.boardHeight;
    }

    /**
     * @brief Gets the width of the game board.
     * @return The width of the game board.
     */
    public int getWidth() {
        return this.boardWidth;
    }

    /**
     * @brief Checks for collisions between the snake and other game elements.
     * @param localSnake The snake to check collisions for.
     * @return The type of collision detected.
     */
    public helperVariables.collisionType checkCollision(Snake localSnake) {
        for (SnakeSegment SnakePart : localSnake.snakeBody) {
            if (localSnake.snakeHead.collisionDetection(SnakePart) == helperVariables.collisionType.snake) {
                System.out.println("Snake body Collision Detected");
                return helperVariables.collisionType.snake;
            }
        }
        for (SnakeSegment SnakePart : snakeAI.snakeBody) {
            if (localSnake.snakeHead.collisionDetection(SnakePart) == helperVariables.collisionType.snake) {
                System.out.println("Snake body Collision Detected");
                return helperVariables.collisionType.snake;
            }
        }

        if (food.collisionDetection(localSnake.snakeHead) == helperVariables.collisionType.food) {
            return helperVariables.collisionType.food;
        }

        for (Wall wall : walls) {
            if (wall.checkCollision(localSnake.snakeHead) == helperVariables.collisionType.wall) {
                System.out.println("WALL!");
                return helperVariables.collisionType.wall;
            }
        }

        return helperVariables.collisionType.none;
    }

    /**
     * @brief Gets the current score of the player.
     * @return The current score.
     */
    private int getCurrentScore() {
        return snake.snakeBody.size();
    }

    /**
     * @brief Gets the player's snake.
     * @return The player's snake.
     */
    public Snake getSnake() {
        return this.snake;
    }

    /**
     * @brief Gets the food item.
     * @return The food item.
     */
    public Food getFood() {
        return this.food;
    }

    /**
     * @brief Gets the list of walls on the game board.
     * @return The list of walls.
     */
    public ArrayList<Wall> getWalls() {
        return this.walls;
    }

    /**
     * @brief Handles game over logic.
     * @throws IOException if an I/O error occurs.
     */
    private void gameOver() throws IOException {
        JLabel gameOverLabel = new JLabel("Game Over");
        gameOverLabel.setFont(new Font("Verdana", Font.BOLD, 16));
        gameOverLabel.setForeground(Color.WHITE);
        gameOverLabel.setBounds(250, 260, 200, 50);

        JLabel scoreLabel = new JLabel("Score: " + snake.snakeBody.size());
        scoreLabel.setFont(new Font("Verdana", Font.BOLD, 16));
        scoreLabel.setForeground(Color.WHITE);
        scoreLabel.setBounds(265, 280, 200, 50);

        add(gameOverLabel);
        add(scoreLabel);
        leaderBoard.saveScore(GameController.playerNick, getCurrentScore());

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Runnable gameOverTask = GameController::switchToGameOverScreen;
        executor.schedule(gameOverTask, 2, TimeUnit.SECONDS);
        executor.shutdown();
    }

    /**
     * @brief Handles the game loop and collision checks.
     * @param e The ActionEvent triggered by the timer.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
        snakeAI.update();
        snake.move();
        helperVariables.collisionType collisonStatePlayer = checkCollision(snake);
        helperVariables.collisionType collisonStateAI = checkCollision(snakeAI);
        if (collisonStatePlayer != helperVariables.collisionType.none && collisonStatePlayer != helperVariables.collisionType.food) {
            isGameOver = true;
            this.gameLoop.stop();
            try {
                gameOver();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        if (collisonStateAI == helperVariables.collisionType.food) {
            snakeAI.addSegment(food);
            placeFood();
        }
        if (collisonStatePlayer == helperVariables.collisionType.food) {
            snake.addSegment(food);
            placeFood();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * @brief Handles key press events to control the snake.
     * @param e The KeyEvent triggered by key press.
     */
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_UP && this.snake.velocityY != 1) {
            this.snake.velocityX = 0;
            this.snake.velocityY = -1;
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN && this.snake.velocityY != -1) {
            this.snake.velocityX = 0;
            this.snake.velocityY = 1;
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT && this.snake.velocityX != 1) {
            this.snake.velocityX = -1;
            this.snake.velocityY = 0;
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT && this.snake.velocityX != -1) {
            this.snake.velocityX = 1;
            this.snake.velocityY = 0;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
