/**
 * @class Food
 * @brief A class representing a food item on the game board.
 * @extends Tile
 */
public class Food extends Tile {
    /**
     * @brief Constructor to initialize a food item at a specific grid position.
     * @param x The x-coordinate on the grid.
     * @param y The y-coordinate on the grid.
     */
    Food(int x, int y) {
        super(x * helperVariables.tileSize, y * helperVariables.tileSize);
    }

    /**
     * @brief Detects collision with a snake segment.
     * @param tile The snake segment to check for collision.
     * @return The type of collision detected.
     */
    @Override
    public helperVariables.collisionType collisionDetection(SnakeSegment tile) {
        if (tile.x == this.x && tile.y == this.y)
            return helperVariables.collisionType.food;

        return helperVariables.collisionType.none;
    }
}
