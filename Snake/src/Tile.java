/**
 * @class Tile
 * @brief An abstract class representing a tile in a grid.
 */
public abstract class Tile {
    int x;
    int y;

    /**
     * @brief Constructor to initialize the tile with given coordinates.
     * @param x X-coordinate of the tile.
     * @param y Y-coordinate of the tile.
     */
    Tile(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @brief Abstract method to perform collision detection with a snake segment.
     * @param seg The snake segment to check for collision.
     * @return The type of collision as defined in helperVariables.collisionType.
     */
    public abstract helperVariables.collisionType collisionDetection(SnakeSegment seg);

    /**
     * @brief Gets the X-coordinate of the tile.
     * @return The X-coordinate of the tile.
     */
    public int getPosX(){
        return this.x;
    }

    /**
     * @brief Gets the Y-coordinate of the tile.
     * @return The Y-coordinate of the tile.
     */
    public int getPosY(){
        return this.y;
    }

}
