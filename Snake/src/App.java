/**
 * @class App
 * @brief The main entry point for the game application.
 */
public class App {

    /**
     * @brief The main method to start the game.
     * @param args Command-line arguments.
     * @throws Exception if an error occurs during game startup.
     */
    public static void main(String[] args) throws Exception {
        GameController.startGame();
    }
}
