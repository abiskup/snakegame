/**
 * @class SnakeSegment
 * @brief A class representing a segment of the snake.
 * @extends Tile
 */
public class SnakeSegment extends Tile {

    /**
     * @brief Constructor to initialize a snake segment at a specific position.
     * @param x The x-coordinate of the segment's position.
     * @param y The y-coordinate of the segment's position.
     */
    SnakeSegment(int x, int y) {
        super(x, y);
    }

    /**
     * @brief Detects collision with another snake segment.
     * @param seg The snake segment to check for collision.
     * @return The type of collision detected.
     */
    @Override
    public helperVariables.collisionType collisionDetection(SnakeSegment seg) {
        if (seg.x == this.x && seg.y == this.y)
            return helperVariables.collisionType.snake;

        return helperVariables.collisionType.none;
    }
}
