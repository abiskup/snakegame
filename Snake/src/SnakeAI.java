import java.awt.Color;
import java.awt.Graphics;
import java.util.Arrays;
import java.util.List;

/**
 * @class SnakeAI
 * @brief A class representing an AI-controlled snake in the game.
 * @extends Snake
 */
public class SnakeAI extends Snake {
    private SnakeGame snakeGame; /**< Reference to the SnakeGame instance */
    private Algo pathfinding; /**< Pathfinding algorithm instance */
    private List<int[]> currentPath; /**< Current path to follow */
    private int currentPathIndex; /**< Index of the current position in the path */

    /**
     * @brief Constructor to initialize the SnakeAI with the game instance.
     * @param snakeGame The SnakeGame instance.
     */
    public SnakeAI(SnakeGame snakeGame) {
        super(15, 15);
        this.snakeGame = snakeGame;
        this.pathfinding = new Algo();
        this.currentPath = null;
        this.currentPathIndex = 0;
    }

    /**
     * @brief Draws the path that the snake AI will follow.
     * @param g The Graphics object to draw on.
     */
    public void drawPath(Graphics g) {
        g.setColor(new Color(122, 108, 41));
        g.fillRect(snakeHead.x, snakeHead.y, helperVariables.tileSize, helperVariables.tileSize);
        g.setColor(new Color(2, 166, 21));
        for (int i = 0; i < currentPath.size(); i++) {
            int[] tmp = currentPath.get(i);
            g.fillRect(tmp[0], tmp[1], helperVariables.tileSize, helperVariables.tileSize);
        }
    }

    /**
     * @brief Updates the snake AI's state, moving it along the path.
     */
    public void update() {
        int[] snakeHeadPos = {getHead().x / helperVariables.tileSize, getHead().y / helperVariables.tileSize};

        // Check if we need to calculate a new path
        if (currentPath == null || currentPathIndex >= currentPath.size() || hasReachedTarget(snakeHeadPos)) {
            int[] fruitPos = {snakeGame.getFood().getPosX() / helperVariables.tileSize, snakeGame.getFood().getPosY() / helperVariables.tileSize};  // Assume only one fruit for simplicity
            currentPath = pathfinding.findPath(snakeHeadPos, fruitPos, snakeGame.getWidth(), snakeGame.getHeight(), snakeGame.getWalls(), getSnakeBody());
            currentPathIndex = 1;  // Reset path index to start moving along the new path
            if (currentPath.isEmpty()) {
                System.out.println("No path found to the fruit!");
                return;
            }
        }

        // Move snake along the path
        if (currentPathIndex < currentPath.size()) {
            int[] nextPos = currentPath.get(currentPathIndex);  // Get the next position in the path

            // Calculate velocity based on next position
            int nextX = nextPos[0] * helperVariables.tileSize;
            int nextY = nextPos[1] * helperVariables.tileSize;

            if (nextX != getHead().x) {
                velocityX = Integer.compare(nextX, getHead().x);
                velocityY = 0;
            } else if (nextY != getHead().y) {
                velocityX = 0;
                velocityY = Integer.compare(nextY, getHead().y);
            }

            move();
            currentPathIndex++;
        }
    }

    /**
     * @brief Checks if the snake AI has reached its target position.
     * @param snakeHeadPos The current position of the snake's head.
     * @return True if the snake has reached the target position, false otherwise.
     */
    private boolean hasReachedTarget(int[] snakeHeadPos) {
        int[] targetPos = currentPath.get(currentPath.size() - 1);
        return Arrays.equals(snakeHeadPos, targetPos);
    }
}
