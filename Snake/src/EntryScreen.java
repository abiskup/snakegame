import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * @class EntryScreen
 * @brief A class representing the entry screen panel for the game.
 */
public class EntryScreen extends JPanel implements ActionListener {
    private JTextField nickField; /**< Text field for entering the nickname */

    /**
     * @brief Constructor to initialize the entry screen panel.
     */
    public EntryScreen() {
        setLayout(null);
        setBackground(helperVariables.entryScrennBackground);
        addElements();
    }

    /**
     * @brief Adds UI elements to the entry screen panel.
     */
    private void addElements(){
        JButton start_button = new JButton("<html><p style='font-size:20px; color:white'>Start game</p></html>");
        start_button.setBounds(150, 400, 300, 100);
        start_button.setBackground(helperVariables.startButtonColor);
        start_button.setFocusable(false);
        start_button.addActionListener(this);
        add(start_button);

        JLabel nick_label = new JLabel("<html><p style='font-size:16px; color:black'>Nickname: </p></html>");
        nick_label.setBounds(250, 260, 200, 50);
        add(nick_label);

        nickField = new JTextField();
        nickField.addActionListener(this);
        nickField.setFont(new Font("Verdana", Font.BOLD, 30));
        nickField.setForeground(Color.black);
        nickField.setBounds(150, 300, 300, 50);
        add(nickField);
    }

    /**
     * @brief Handles action events such as button clicks and text field actions.
     * @param e The action event.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (nickField.getText().isEmpty()) {
            nickField.setBackground(Color.red);
            return;
        }
        GameController.playerNick = nickField.getText();
        try {
            GameController.switchToGameBoard();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
