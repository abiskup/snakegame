import java.awt.*;

/**
 * @class helperVariables
 * @brief A class containing helper variables and enums used throughout the game.
 */
public class helperVariables {

    /**
     * @enum collisionType
     * @brief Enum representing different types of collisions.
     */
    enum collisionType {none, snake, food, wall}

    static int tileSize = 25; /**< Size of each tile in the game */
    static int boardHeight = 600; /**< Height of the game board */
    static int boardWidth = 600; /**< Width of the game board */
    public static String leaderBoardFileName = "leaderboard.csv"; /**< File path for the leaderboard */

    public static Color entryScrennBackground = new Color(0x60676B); /**< Background color for the entry screen */
    public static Color startButtonColor = new Color(0xDAA520); /**< Color for the start button */
}
