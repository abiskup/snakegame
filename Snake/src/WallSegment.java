import java.awt.Color;
import java.awt.Graphics;

/**
 * @class WallSegment
 * @brief A class representing a segment of a wall.
 * @extends Tile
 */
public class WallSegment extends Tile {

    /**
     * @brief Constructor to initialize a wall segment at a specific position.
     * @param x The x-coordinate of the segment's position.
     * @param y The y-coordinate of the segment's position.
     */
    WallSegment(int x, int y) {
        super(x * helperVariables.tileSize, y * helperVariables.tileSize);
    }

    /**
     * @brief Detects collision with a snake segment.
     * @param tile The snake segment to check for collision.
     * @return The type of collision detected.
     */
    @Override
    public helperVariables.collisionType collisionDetection(SnakeSegment tile) {
        if (tile.x == this.x && tile.y == this.y)
            return helperVariables.collisionType.wall;

        return helperVariables.collisionType.none;
    }

    /**
     * @brief Draws the wall segment on the screen.
     * @param g The Graphics object to draw on.
     */
    public void draw(Graphics g) {
        g.setColor(Color.gray);
        g.fillRect(this.x, this.y, helperVariables.tileSize, helperVariables.tileSize);
    }
}
