import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

/**
 * @class Wall
 * @brief A class representing a wall composed of multiple wall segments.
 */
public class Wall {
    int wallLength; /**< Length of the wall */
    Random random; /**< Random instance for generating positions */
    ArrayList<WallSegment> wall; /**< List of wall segments that make up the wall */

    /**
     * @brief Constructor to initialize a wall with a given length and direction.
     * @param length The length of the wall.
     * @param dir The direction of the wall ("vertical" or "horizontal").
     */
    Wall(int length, String dir) {
        wall = new ArrayList<>();
        random = new Random();
        int h = random.nextInt(helperVariables.boardHeight / helperVariables.tileSize);
        int l = random.nextInt(helperVariables.boardWidth / helperVariables.tileSize);
        if (dir.equals("vertical")) {
            for (int i = 0; i < length; ++i) {
                wall.add(new WallSegment(l, h + i));
            }
        }
        if (dir.equals("horizontal")) {
            for (int i = 0; i < length; ++i) {
                wall.add(new WallSegment(l + i, h));
            }
        }
    }

    /**
     * @brief Constructor to initialize a wall with a given length, direction, and starting position.
     * @param length The length of the wall.
     * @param dir The direction of the wall ("vertical" or "horizontal").
     * @param posx The x-coordinate of the starting position.
     * @param posy The y-coordinate of the starting position.
     */
    Wall(int length, String dir, int posx, int posy) {
        wall = new ArrayList<>();
        if (dir.equals("vertical")) {
            for (int i = 0; i < length; ++i) {
                wall.add(new WallSegment(posx, posy + i));
            }
        }
        if (dir.equals("horizontal")) {
            for (int i = 0; i < length; ++i) {
                wall.add(new WallSegment(posx + i, posy));
            }
        }
    }

    /**
     * @brief Draws the wall on the screen.
     * @param g The Graphics object to draw on.
     */
    public void draw(Graphics g) {
        for (WallSegment wallSegment : wall) {
            wallSegment.draw(g);
        }
    }

    /**
     * @brief Checks for collision with a given snake segment.
     * @param seg The snake segment to check for collision.
     * @return The type of collision detected.
     */
    public helperVariables.collisionType checkCollision(SnakeSegment seg) {
        helperVariables.collisionType collision = helperVariables.collisionType.none;
        for (WallSegment wallSegment : wall) {
            collision = wallSegment.collisionDetection(seg);
            if (collision != helperVariables.collisionType.none) {
                return collision;
            }
        }
        return collision;
    }

    /**
     * @brief Gets the x-coordinate of the first segment of the wall.
     * @return The x-coordinate of the first wall segment.
     */
    public int getPosX() {
        return wall.get(0).getPosX();
    }

    /**
     * @brief Gets the y-coordinate of the last segment of the wall.
     * @return The y-coordinate of the last wall segment.
     */
    public int getPosY() {
        return wall.get(wall.size() - 1).getPosY();
    }
}
