import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Date;

/**
 * @class LeaderBoard
 * @brief A class for managing the leaderboard of the game.
 */
public class LeaderBoard {
    private List<String[]> dataFromFile; /**< List to store leaderboard data from the file */
    String filename; /**< Filename of the leaderboard file */

    /**
     * @brief Constructor to initialize LeaderBoard with a filename.
     * @param filename The name of the file containing the leaderboard data.
     * @throws IOException if an I/O error occurs.
     */
    LeaderBoard(String filename) throws IOException {
        CsvHandler csvHandler = new CsvHandler(filename);
        this.filename = filename;
        this.dataFromFile = new ArrayList<>(Arrays.asList(csvHandler.readDataFromFile()));
    }

    /**
     * @brief Reloads the leaderboard data from the file.
     * @throws IOException if an I/O error occurs.
     */
    private void reloadDataFromFile() throws IOException {
        CsvHandler csvHandler = new CsvHandler(filename);
        this.dataFromFile = new ArrayList<>(Arrays.asList(csvHandler.readDataFromFile()));
    }

    /**
     * @brief Checks if a nickname is already present in the leaderboard.
     * @param nick The nickname to check.
     * @return The index of the nickname if found, -1 otherwise.
     */
    private int isAlreadyInTable(String nick) {
        for (int i = 0; i < dataFromFile.size(); i++) {
            String[] strings = dataFromFile.get(i);
            if (strings[0].equals(nick)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * @brief Updates the leaderboard file with the current data.
     * @throws IOException if an I/O error occurs.
     */
    private void updateLeaderBoardFile() throws IOException {
        String[][] currentLeaderBoardData = dataFromFile.toArray(new String[dataFromFile.size()][]);
        CsvHandler csvHandler = new CsvHandler(filename);
        csvHandler.writeBulkData(currentLeaderBoardData);
    }

    /**
     * @brief Sorts the leaderboard data in descending order based on scores.
     */
    private void sortLeaderBoard() {
        dataFromFile = dataFromFile.stream()
                .filter(row -> row.length > 1)
                .sorted(Comparator.comparing(row -> Integer.parseInt(row[1]), Comparator.reverseOrder()))
                .collect(Collectors.toList());
    }

    /**
     * @brief Saves a score to the leaderboard.
     * @param nick The nickname of the player.
     * @param score The score of the player.
     * @throws IOException if an I/O error occurs.
     */
    public void saveScore(String nick, int score) throws IOException {
        if (score < 0) {
            throw new IllegalArgumentException("Score cannot be negative");
        }
        if (nick == null) {
            throw new IllegalArgumentException("Nick cannot be null");
        }
        Date date = new Date();
        String timestamp = date.toString();
        int existingRowIndex = isAlreadyInTable(nick);
        if (existingRowIndex != -1) {
            String[] row = dataFromFile.get(existingRowIndex);
            if (Integer.parseInt(row[1]) < score) {
                row[1] = String.valueOf(score);
                row[2] = timestamp;
            }
        } else {
            String[] row = new String[3];
            row[0] = nick;
            row[1] = String.valueOf(score);
            row[2] = timestamp;
            dataFromFile.add(row);
        }
        sortLeaderBoard();
        updateLeaderBoardFile();
    }

    /**
     * @brief Erases all data from the leaderboard.
     * @throws IOException if an I/O error occurs.
     */
    public void eraseLeaderBoard() throws IOException {
        dataFromFile.clear();
        updateLeaderBoardFile();
    }

    /**
     * @brief Gets the leaderboard data.
     * @return A 2D array of strings representing the leaderboard data.
     * @throws IOException if an I/O error occurs.
     */
    public String[][] getLeaderBoard() throws IOException {
        reloadDataFromFile();
        return dataFromFile.toArray(new String[dataFromFile.size()][]);
    }
}
