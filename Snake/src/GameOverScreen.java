import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

/**
 * @class GameOverScreen
 * @brief A class representing the game over screen panel.
 */
public class GameOverScreen extends JPanel {

    /**
     * @brief Constructor to initialize the game over screen panel.
     */
    public GameOverScreen() {
        setLayout(null);
        setBackground(Color.black);
        addElements();
    }

    /**
     * @brief Adds UI elements to the game over screen panel.
     */
    private void addElements() {
        int buttonWidth = 200;  // Width of each button
        int buttonHeight = 50;  // Height of each button

        // Calculate the x position to center the button
        int centerX = (600 - buttonWidth) / 2;
        Color buttonColor = new Color(70, 180, 94);

        // Retry button
        JButton retry = new JButton("Retry");
        retry.setBounds(centerX, 150, buttonWidth, buttonHeight);
        retry.setFocusable(false);
        retry.setBackground(buttonColor);
        retry.setForeground(Color.WHITE);
        retry.setFont(new Font("Verdana", Font.BOLD, 16));
        retry.setOpaque(true);
        retry.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2, true));
        retry.setFocusPainted(false);
        retry.setContentAreaFilled(true);

        // Add hover effect for retry button
        retry.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                retry.setBackground(Color.DARK_GRAY);  // Change color on hover
            }

            @Override
            public void mouseExited(MouseEvent e) {
                retry.setBackground(buttonColor);  // Revert to original color
            }
        });

        retry.addActionListener(e -> {
            try {
                GameController.switchToGameBoard();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        add(retry);

        // Leaderboard button
        JButton leaderboard = new JButton("Leaderboard");
        leaderboard.setBounds(centerX, 225, buttonWidth, buttonHeight);
        leaderboard.setFocusable(false);
        leaderboard.setBackground(buttonColor);
        leaderboard.setForeground(Color.WHITE);
        leaderboard.setFont(new Font("Verdana", Font.BOLD, 16));
        leaderboard.setOpaque(true);
        leaderboard.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2, true));
        leaderboard.setFocusPainted(false);
        leaderboard.setContentAreaFilled(true);

        // Add hover effect for leaderboard button
        leaderboard.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                leaderboard.setBackground(Color.DARK_GRAY);  // Change color on hover
            }

            @Override
            public void mouseExited(MouseEvent e) {
                leaderboard.setBackground(buttonColor);  // Revert to original color
            }
        });

        leaderboard.addActionListener(e -> {
            try {
                GameController.switchToLeaderBoardScreen();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        add(leaderboard);

        // Quit button
        JButton quitButton = new JButton("Quit");
        quitButton.setBounds(centerX, 300, buttonWidth, buttonHeight);
        quitButton.setFocusable(false);
        quitButton.setBackground(buttonColor);
        quitButton.setForeground(Color.WHITE);
        quitButton.setFont(new Font("Verdana", Font.BOLD, 16));
        quitButton.setOpaque(true);
        quitButton.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2, true));
        quitButton.setFocusPainted(false);
        quitButton.setContentAreaFilled(true);

        // Add hover effect for quit button
        quitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                quitButton.setBackground(Color.DARK_GRAY);  // Change color on hover
            }

            @Override
            public void mouseExited(MouseEvent e) {
                quitButton.setBackground(buttonColor);  // Revert to original color
            }
        });

        quitButton.addActionListener(e -> System.exit(0));

        add(quitButton);
    }
}
