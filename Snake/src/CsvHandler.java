import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @class CsvHandler
 * @brief A class for handling CSV file operations such as reading and writing.
 */
public class CsvHandler {
    private final String filename;

    /**
     * @param filename The name of the CSV file to handle.
     * @throws IOException if an I/O error occurs.
     * @brief Constructor to initialize CsvHandler with a filename.
     */
    CsvHandler(String filename) throws IOException {
        this.filename = filename;
    }

    /**
     * @param line An array of strings representing the values to write in the CSV line.
     * @brief Writes a single line to the CSV file.
     */
    public void writeLine(String[] line) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(this.filename, true))) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < line.length; i++) {
                String s = line[i];
                sb.append(s);
                if (i < line.length - 1) {
                    sb.append(",");
                }
            }
            sb.append("\n");
            bw.write(sb.toString());
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param data A 2D array of strings representing the data to write in the CSV file.
     * @brief Writes bulk data to the CSV file.
     */
    public void writeBulkData(String[][] data) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(this.filename))) {


            for (int i = 0; i < data.length; i++) {
                StringBuilder sb = new StringBuilder();
                for (int j = 0; j < data[i].length; j++) {
                    String s = data[i][j];
                    sb.append(s);
                    if (j < data[i].length - 1) {
                        sb.append(",");
                    }
                }
                sb.append("\n");
                bw.write(sb.toString());
                bw.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return A 2D array of strings representing the data read from the CSV file.
     * @brief Reads data from the CSV file.
     */

    public String[][] readDataFromFile() {
        List<String[]> lines = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(this.filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                lines.add(values);  // Add the split line to the list
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Convert List<String[]> to String[][] for return
        return lines.toArray(new String[0][]);
    }
}


