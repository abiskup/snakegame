import java.awt.*;
import java.util.ArrayList;

/**
 * @class Snake
 * @brief A class representing the snake in the game.
 */
public class Snake {
    int posX, posY; /**< The x and y positions of the snake head */
    int velocityX, velocityY; /**< The velocity of the snake in the x and y directions */
    SnakeSegment snakeHead; /**< The head segment of the snake */
    Color color = Color.green; /**< The color of the snake */
    ArrayList<SnakeSegment> snakeBody; /**< The body segments of the snake */

    /**
     * @brief Constructor to initialize the snake at a specific position.
     * @param x The x-coordinate of the snake's starting position.
     * @param y The y-coordinate of the snake's starting position.
     */
    Snake(int x, int y) {
        System.err.println("kupa");
        velocityX = 0;
        velocityY = 0;
        posX = x * helperVariables.tileSize;
        posY = y * helperVariables.tileSize;
        snakeHead = new SnakeSegment(x * helperVariables.tileSize, y * helperVariables.tileSize);
        snakeBody = new ArrayList<SnakeSegment>();
    }

    /**
     * @brief Gets the snake body segments.
     * @return An ArrayList of SnakeSegment objects representing the snake's body.
     */
    public ArrayList<SnakeSegment> getSnakeBody() {
        return snakeBody;
    }

    /**
     * @brief Moves the snake in the direction of its velocity.
     */
    public void move() {
        // Snake body
        for (int i = snakeBody.size() - 1; i >= 0; i--) {
            SnakeSegment s = snakeBody.get(i);
            if (i == 0) {
                s.x = snakeHead.x;
                s.y = snakeHead.y;
            } else {
                SnakeSegment prevSnakeSegment = snakeBody.get(i - 1);
                s.x = prevSnakeSegment.x;
                s.y = prevSnakeSegment.y;
            }
        }

        // Snake head
        this.posX += velocityX * helperVariables.tileSize;
        this.posY += velocityY * helperVariables.tileSize;
        snakeHead.x = this.posX;
        snakeHead.y = this.posY;
    }

    /**
     * @brief Draws the snake on the screen.
     * @param g The Graphics object to draw on.
     */
    public void draw(Graphics g) {
        g.setColor(new Color(2, 204, 25));
        g.fillRect(snakeHead.x, snakeHead.y, helperVariables.tileSize, helperVariables.tileSize);
        g.setColor(new Color(2, 166, 21));
        for (int i = 0; i < snakeBody.size(); i++) {
            SnakeSegment segment = snakeBody.get(i);
            g.fillRect(segment.x, segment.y, helperVariables.tileSize, helperVariables.tileSize);
        }
    }

    /**
     * @brief Adds a segment to the snake body when food is consumed.
     * @param consumedFood The food item that was consumed.
     */
    public void addSegment(Food consumedFood) {
        System.out.println("FOOD CONSUMED!");
        this.snakeBody.add(new SnakeSegment(consumedFood.x, consumedFood.y));
    }

    /**
     * @brief Gets the head segment of the snake.
     * @return The head segment of the snake.
     */
    public SnakeSegment getHead() {
        return this.snakeHead;
    }

    /**
     * @brief Placeholder for the paintComponent method.
     * @param g The Graphics object to paint on.
     * @throws UnsupportedOperationException as the method is not implemented.
     */
    public void paintComponent(Graphics g) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'paintComponent'");
    }
}
