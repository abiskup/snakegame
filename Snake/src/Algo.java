import java.util.*;

/**
 * @class Algo
 * @brief Class implementing pathfinding algorithm using A* search.
 */
public class Algo {
    /**
     * @class Node
     * @brief A class representing a node in the pathfinding grid.
     */
    public static class Node implements Comparable<Node> {
        int[] position;
        int gCost;
        int hCost;
        Node parent;

        /**
         * @param position Position of the node.
         * @param gCost    Cost from the start node to this node.
         * @param hCost    Heuristic cost from this node to the goal.
         * @param parent   Parent node in the path.
         * @brief Constructor to initialize a node.
         */
        public Node(int[] position, int gCost, int hCost, Node parent) {
            this.position = position;
            this.gCost = gCost;
            this.hCost = hCost;
            this.parent = parent;
        }

        /**
         * @return The sum of gCost and hCost.
         * @brief Gets the total cost (fCost) of the node.
         */
        public int getFCost() {
            return gCost + hCost;
        }

        /**
         * @param other The other node to compare with.
         * @return A negative integer, zero, or a positive integer as this node is less than, equal to, or greater than the specified node.
         * @brief Compares this node with another node based on fCost.
         */
        @Override
        public int compareTo(Node other) {
            return Integer.compare(this.getFCost(), other.getFCost());
        }
    }

    /**
     * @param a The first point as [x, y].
     * @param b The second point as [x, y].
     * @return The estimated cost between point a and point b.
     * @brief Heuristic function to estimate the cost between two points.
     */
    private int heuristic(int[] a, int[] b) {
        return Math.abs(a[0] - b[0]) + Math.abs(a[1] - b[1]);
    }

    /**
     * @param start       The starting position as [x, y].
     * @param goal        The goal position as [x, y].
     * @param boardWidth  The width of the board.
     * @param boardHeight The height of the board.
     * @param obstacles   List of walls on the board.
     * @param snakeBody   List of snake segments on the board.
     * @return The list of positions from start to goal representing the path.
     * @brief Finds the shortest path from start to goal on a grid with obstacles.
     */
    public List<int[]> findPath(int[] start, int[] goal, int boardWidth, int boardHeight, List<Wall> obstacles, List<SnakeSegment> snakeBody) {
        Set<String> obstacleSet = new HashSet<>();
        for (Wall wall : obstacles) {
            for (WallSegment wallSegment : wall.wall) {
                obstacleSet.add(wallSegment.getPosX() / helperVariables.tileSize + "," + wallSegment.getPosY() / helperVariables.tileSize);
            }
        }
        for (SnakeSegment segment : snakeBody) {
            obstacleSet.add((segment.x / helperVariables.tileSize) + "," + (segment.y / helperVariables.tileSize));
        }

        for (String obs : obstacleSet) {
            System.out.println(obs);
        }

        PriorityQueue<Node> openSet = new PriorityQueue<>();
        Map<String, Node> allNodes = new HashMap<>();

        Node startNode = new Node(start, 0, heuristic(start, goal), null);
        openSet.add(startNode);
        allNodes.put(Arrays.toString(start), startNode);

        while (!openSet.isEmpty()) {
            Node current = openSet.poll();

            if (Arrays.equals(current.position, goal)) {
                return reconstructPath(current);
            }

            int[][] neighbors = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
            for (int[] dir : neighbors) {
                int[] neighborPos = {current.position[0] + dir[0], current.position[1] + dir[1]};
                String neighborKey = neighborPos[0] + "," + neighborPos[1];

                if (isValidPosition(neighborPos, boardWidth, boardHeight, obstacleSet)) {
                    int tentativeGCost = current.gCost + 1;
                    Node neighborNode = allNodes.getOrDefault(neighborKey, new Node(neighborPos, Integer.MAX_VALUE, heuristic(neighborPos, goal), current));

                    if (tentativeGCost < neighborNode.gCost) {
                        neighborNode.gCost = tentativeGCost;
                        neighborNode.parent = current;
                        allNodes.put(neighborKey, neighborNode);
                        openSet.add(neighborNode);
                    }
                }
            }
        }
        return Collections.emptyList();
    }

    /**
     * @param pos         The position to check as [x, y].
     * @param boardWidth  The width of the board.
     * @param boardHeight The height of the board.
     * @param obstacleSet The set of obstacles on the board.
     * @return True if the position is valid, false otherwise.
     * @brief Checks if a position is valid on the board.
     */
    private boolean isValidPosition(int[] pos, int boardWidth, int boardHeight, Set<String> obstacleSet) {
        return pos[0] >= 0 && pos[0] < boardWidth && pos[1] >= 0 && pos[1] < boardHeight && !obstacleSet.contains(pos[0] + "," + pos[1]);
    }

    /**
     * @param current The goal node.
     * @return The list of positions from start to goal representing the path.
     * @brief Reconstructs the path from the goal node to the start node.
     */

    private List<int[]> reconstructPath(Node current) {
        List<int[]> path = new ArrayList<>();
        while (current != null) {
            path.add(current.position);
            current = current.parent;
        }
        Collections.reverse(path);
        return path;
    }
}    

