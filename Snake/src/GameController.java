import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * @class GameController
 * @brief The main controller class for managing game screens and transitions.
 * @extends JFrame
 */
public class GameController extends JFrame {
    static JPanel mainPanel; /**< Main panel for card layout to switch between different screens */
    static String playerNick; /**< Player's nickname */

    /**
     * @brief Constructor to initialize the game controller UI.
     */
    public GameController() {
        initUI();
    }

    /**
     * @brief Initializes the user interface of the game controller.
     */
    private void initUI() {
        mainPanel = new JPanel(new CardLayout());
        mainPanel.add(new EntryScreen());
        add(mainPanel);
        setResizable(false);
        mainPanel.setPreferredSize(new Dimension(helperVariables.boardWidth, helperVariables.boardHeight));
        pack();
    }

    /**
     * @brief Switches the main panel to the game board screen.
     * @throws IOException if an I/O error occurs.
     */
    public static void switchToGameBoard() throws IOException {
        SnakeGame snakeGame = new SnakeGame(helperVariables.boardWidth, helperVariables.boardHeight);
        mainPanel.removeAll();
        mainPanel.add(snakeGame);
        mainPanel.revalidate();
        mainPanel.repaint();
        snakeGame.requestFocusInSnakeGamePanel();
    }

    /**
     * @brief Switches the main panel to the game over screen.
     */
    public static void switchToGameOverScreen() {
        GameOverScreen gameOverScreen = new GameOverScreen();
        mainPanel.removeAll();
        mainPanel.add(gameOverScreen);
        mainPanel.revalidate();
        mainPanel.repaint();
        gameOverScreen.requestFocusInWindow();
    }

    /**
     * @brief Switches the main panel to the leaderboard screen.
     * @throws IOException if an I/O error occurs.
     */
    public static void switchToLeaderBoardScreen() throws IOException {
        LeaderBoard leaderBoard = new LeaderBoard(helperVariables.leaderBoardFileName);
        String[][] leaderBoardData = leaderBoard.getLeaderBoard();
        LeaderBoardScreen leaderBoardScreen = new LeaderBoardScreen(leaderBoardData);

        mainPanel.removeAll();
        mainPanel.add(leaderBoardScreen);
        mainPanel.revalidate();
        mainPanel.repaint();
    }

    /**
     * @brief Starts the game by creating a new GameController instance and making it visible.
     */
    public static void startGame() {
        JFrame newGame = new GameController();
        newGame.setVisible(true);
    }
}
