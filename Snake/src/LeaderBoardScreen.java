import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * @class LeaderBoardScreen
 * @brief A class representing the leaderboard screen panel.
 * @extends JPanel
 */
public class LeaderBoardScreen extends JPanel {

    /**
     * @brief Constructor to initialize the leaderboard screen with data.
     * @param data A 2D array of strings representing the leaderboard data.
     */
    public LeaderBoardScreen(String[][] data) {
        setLayout(null);
        setBackground(Color.black);
        addElements(data);
    }

    /**
     * @brief Adds UI elements to the leaderboard screen panel.
     * @param data A 2D array of strings representing the leaderboard data.
     */
    private void addElements(String[][] data) {
        int buttonWidth = 200;  // Width of each button
        int buttonHeight = 50;  // Height of each button

        // Calculate the x position to center the button
        int centerX = (600 - buttonWidth) / 2;

        // Title label
        JLabel titleLabel = new JLabel("Leaderboard", SwingConstants.CENTER);
        titleLabel.setFont(new Font("Verdana", Font.BOLD, 24));
        titleLabel.setForeground(Color.WHITE);
        titleLabel.setBounds(150, 20, 300, 40);
        add(titleLabel, BorderLayout.NORTH);

        // Column names for the table
        String[] columnNames = {"Nick", "Score", "Timestamp"};

        // Create the table with the data and column names
        DefaultTableModel model = new DefaultTableModel(data, columnNames);
        JTable leaderboardTable = new JTable(model);
        leaderboardTable.setFillsViewportHeight(true);
        leaderboardTable.setBackground(new Color(60, 63, 65));
        leaderboardTable.setForeground(Color.WHITE);
        leaderboardTable.setGridColor(Color.GRAY);
        leaderboardTable.setFont(new Font("Verdana", Font.PLAIN, 16));
        leaderboardTable.setRowHeight(30);

        // Table headers appearance
        leaderboardTable.getTableHeader().setBackground(new Color(203, 160, 52));
        leaderboardTable.getTableHeader().setForeground(Color.WHITE);
        leaderboardTable.getTableHeader().setFont(new Font("Verdana", Font.BOLD, 16));

        // Scroll pane for table
        JScrollPane scrollPane = new JScrollPane(leaderboardTable);
        scrollPane.setBounds(50, 80, 500, 400);
        add(scrollPane, BorderLayout.CENTER);

        // Back button
        JButton back = new JButton("Back");
        back.setBounds(centerX, 500, buttonWidth, buttonHeight);
        back.setFocusable(false);
        back.addActionListener(e -> GameController.switchToGameOverScreen());
        add(back, BorderLayout.SOUTH);
    }
}
