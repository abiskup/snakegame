# Snake Game

## Overview

This project is a classic Snake game implemented in Java using the Swing library for the graphical user interface. The game includes a player-controlled snake and an AI-controlled snake that compete to collect food on the game board. The game ends when a snake collides with itself, a wall, or another snake. The project also features a leaderboard that records player scores.

## Features

- **Player-controlled snake**: Move the snake using arrow keys to collect food and grow.
- **AI-controlled snake**: An AI snake that uses the A* algorithm to find the shortest path to the food.
- **Collision detection**: The game checks for collisions with walls, the snake's own body, and other snakes.
- **Graphical interface**: The game uses Swing for rendering graphics and managing the user interface.
- **Leaderboard**: Player scores are recorded and displayed on a leaderboard.
- **Various screens**: Includes an entry screen for player nickname, game over screen, and leaderboard screen.

## Classes and Structure

- **App**: Entry point of the game that starts the game.
- **GameController**: Manages switching between different screens and overall game control.
- **EntryScreen**: Initial screen where the player can enter their nickname.
- **SnakeGame**: Contains the main game logic, including snake movement, collision detection, and rendering.
- **Snake** and **SnakeSegment**: Represents the player-controlled snake and its segments.
- **SnakeAI**: Extends `Snake` to implement AI behavior for the AI-controlled snake.
- **Tile**, **Food**, **Wall**, and **WallSegment**: Represents elements of the game board.
- **LeaderBoard** and **CsvHandler**: Manages the leaderboard and handles CSV file operations.
- **helperVariables**: Contains constants and enums used throughout the game.

## Getting Started

### Prerequisites

- Java Development Kit (JDK)
- An IDE or text editor (e.g., IntelliJ IDEA, Eclipse, VSCode)

### Running the Game

1. Clone the repository:
   ```bash
   git clone https://github.com/yourusername/SnakeGame.git
   cd SnakeGame

    Open the project in your preferred IDE.

    Run the App class to start the game.

Generating Documentation with Doxygen

This project includes Doxygen comments for generating documentation. Follow the steps below to generate the documentation:
Prerequisites

    Doxygen must be installed on your system.

Steps



Run Doxygen in the main folder where the Doxyfile is located:



    doxygen Doxyfile

    
The documentation will be generated in the docs folder. Open the index.html file in the docs/html directory to view the documentation.
